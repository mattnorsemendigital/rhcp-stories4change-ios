//
//  ViewController.swift
//  Stories For Change
//
//  Created by Matt Lien on 7/31/18.
//  Copyright © 2018 Rochester Healthy Community Partnership. All rights reserved.
//

import UIKit
import ResearchKit

class IntroductionViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func joinStudyAction(_ sender: Any) {
        let taskViewController = ORKTaskViewController(task: ConsentTask, taskRun: nil)
        taskViewController.delegate = self
        present(taskViewController, animated: true, completion: nil)
    }
}

