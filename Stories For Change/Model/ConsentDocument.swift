//
//  ConsentDocument.swift
//  Stories For Change
//
//  Created by Matt Lien on 7/31/18.
//  Copyright © 2018 Rochester Healthy Community Partnership. All rights reserved.
//

import Foundation
import ResearchKit

class ConsentDocument: ORKConsentDocument {
    
    //MARK: Initialization
    
    override init() {
        super.init()
        
        title = NSLocalizedString("", comment: "")
        
        let sectionTypes: [ORKConsentSectionType] = [
            .overview,
            .dataGathering,
            .privacy,
            .dataUse,
            .timeCommitment,
            .studySurvey,
            .studyTasks,
            .withdrawing
        ]
        
        var count = 0
        
        //map the types to the sections
        let consentSections: [ORKConsentSection] = sectionTypes.map { contentSectionType in
            let section = ORKConsentSection(type: contentSectionType)
            
            switch contentSectionType {
            case .overview:
                section.summary = NSLocalizedString("The purpose of this study is to understand... The next few screens will explain the study and allow you to provide an electronic consent to participate.", comment: "")
                
                section.customLearnMoreButtonTitle = NSLocalizedString("", comment: "")
                
            case .dataGathering:
                section.summary = NSLocalizedString("You will be in this study for 6 months.", comment: "")
                
                section.content = NSLocalizedString("This section describes how the data is gathered...", comment: "")
                
            case .privacy:
                section.summary = NSLocalizedString("Rochester Healthy Community Partnership is committed to protecting the confidentiality of information obtained about you in connection with this research study.", comment: "")
                
                section.content = NSLocalizedString("To protect the data and confidentiality of your data, a code will be used as an identifier. The code will be a registration number assigned specifically to the patient by the Rochester Healthy Community Partnership Study Team. Under Federal law called the Privacy Rule, health information is private.  However, there are exceptions to this rule, and you should know who may be able to see, use and share your health information for research and why they may need to do so.  Information about you and your health cannot be used in this research study without your written permission.  If you sign this form, it will provide that permission.\n\nHealth information may be collected about you from:\n- Past, present and future medical records.\n- Research procedures, including research office visits, tests, interviews and questionnaires.\n\nWhy will this information be used and/or given to others?\n- To do the research.\n- To report the results.\n- To see if the research was done correctly.\n\nIf the results of this study are made public, information that identifies you will not be used.\n\nWho may use or share your health information?\n- Rochester Healthy Community Partnership research staff involved in this study.\n\nWith whom may your health information be shared?\n- The Rochester Healthy Community Partnership Institutional Review Board that oversees the research.\n- Federal and State agencies (such as the Food and Drug Administration, the Department of Health and Human Services, the National Institutes of Health and other United States agencies) or government agencies in other countries that oversee or review research.\n\nIs your health information protected after it has been shared with others?\nRochester Healthy Community Partnership asks anyone who receives your health information from us to protect your privacy; however, once your information is shared outside Rochester Healthy Community Partnership, we cannot promise that it will remain private and it may no longer be protected by the Privacy Rule.\n\nYour Privacy Rights\nYou do not have to sign this form, but if you do not, you cannot take part in this research study.\n\nIf you cancel your permission to use or share your health information, your participation in this study will end and no more information about you will be collected; however, information already collected about you in the study may continue to be used.\n\nIf you choose not to take part or if you withdraw from this study, it will not harm your relationship with your own doctors or with Rochester Healthy Community Partnership.\n\nYou may cancel your permission by emailing the Rochester Healthy Community Partnership Research Subject Advocate at: rhcp@rhcp.org\n\nPlease be sure to include in your email:\n- The name of the Principal Investigator,\n- The study IRB number and /or study name, and\n- Your contact information.\n\nYour permission lasts until the end of this study, unless you cancel it.  Because research is an ongoing process, we cannot give you an exact date when the study will end.", comment: "")
                
            case .dataUse:
                section.summary = NSLocalizedString("Your coded study data will be used for research at Rochester Healthy Community Partnership. It will be combined with data from other participants for analysis.", comment: "")
                
                section.content = NSLocalizedString("By combining your data with data from other participants, researchers have a rich database of information that can lead to discoveries about measuring quality of life in cancer patients.", comment: "")
                
            case .timeCommitment:
                
                section.summary = NSLocalizedString("You will be in this study for 6 months. During this time, you will be asked to complete questions about your retention of information from the Stories For Change videos. These questions will take about 5 minutes each month.", comment: "")
                
                section.content = NSLocalizedString("If you take part in this research, you will be responsible to complete the questionnaires.", comment: "")
                
                section.customLearnMoreButtonTitle = NSLocalizedString("Learn more about how much time the study takes", comment: "")
                
                
            case .studySurvey:
                
                //Risks Screen
                section.title = NSLocalizedString("Risks", comment: "")
                section.customLearnMoreButtonTitle = NSLocalizedString("Learn more about risks", comment: "")
                
                section.summary = NSLocalizedString("As with all research, there is a chance that confidentiality could be compromised; however, we take precautions to minimize this risk.", comment: "")
                
                section.content = NSLocalizedString("If you think you have suffered a research-related injury, you should promptly notify the Principal Investigator.\n\nWho will pay for the treatment of research related injuries:\nCare for such research-related injuries will be billed in the ordinary manner, to you or your insurance.  You will be responsible for all treatment costs not covered by your insurance, including deductibles, co-payments and coinsurance.", comment: "")
                
                
            case .studyTasks:
                
                //Costs & Compensation Screen
                section.title = NSLocalizedString("Costs and Compensation", comment: "")
                //section.customLearnMoreButtonTitle = NSLocalizedString("Learn more about costs and compensation", comment: "")
                
                section.summary = NSLocalizedString("You won’t be paid for taking part in this study.", comment: "")
                
//                section.content = NSLocalizedString("Mayo Clinic is funding the study.\n\nYou won’t need to pay for the Apple Watch, which is provided just for this research study.\n\nHowever, you and/or your insurance will need to pay for all other tests and procedures that you would have as part of your clinical care, including co-payments and deductibles.", comment: "")
//
                
            case .withdrawing:
                section.summary = NSLocalizedString("You may decide to stop at any time.  You should tell the Principal Investigator if you decide to stop.", comment: "")
                
                section.content = NSLocalizedString("The Principal Investigator or Rochester Healthy Community Partnership may stop you from taking part in this study at any time:\n- if it is in your best interest,\n- if you don’t follow the study procedures,\n- if the study is stopped.\n\nIf you leave this research study early, or are withdrawn from the study, no more information about you will be collected; however, information already collected about you in the study may continue to be used.\n\nWe will tell you about any new information that may affect your willingness to stay in the research study.", comment: "")
                
            default:
                break
            }
            
            count += 1
            return section
        }
        
        //assign the consent sections to the sections object
        sections = consentSections
        
        
        //add the signature
        let signature = ORKConsentSignature(forPersonWithTitle: nil, dateFormatString: nil, identifier: "ConsentDocumentParticipantSignature")
        addSignature(signature)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ORKConsentSectionType: CustomStringConvertible {
    
    public var description: String {
        
        switch self {
            
        case .overview:
            return "overview"
            
        case .dataGathering:
            return "dataGathering"
            
        case .privacy:
            return "privacy"
            
        case .dataUse:
            return "dataUse"
            
        case .timeCommitment:
            return "timeCommitment"
            
        case .studySurvey:
            return "studySurvey"
            
        case .studyTasks:
            return "studyTasks"
            
        case .withdrawing:
            return "withdrawing"
            
        case .custom:
            return "custom"
            
        case .onlyInDocument:
            return "onlyInDocument"
            
        }
    }
}
