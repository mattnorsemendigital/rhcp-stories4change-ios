//
//  ViewControllerExtension.swift
//  Stories For Change
//
//  Created by Matt Lien on 7/31/18.
//  Copyright © 2018 Rochester Healthy Community Partnership. All rights reserved.
//

import ResearchKit
import UIKit

extension UIViewController : ORKTaskViewControllerDelegate {
    
    public func taskViewController(_ taskViewController: ORKTaskViewController, stepViewControllerWillAppear stepViewController: ORKStepViewController) {
        UIView.appearance().tintColor =  UIColor(red: 47.0/255.0, green: 94.0/255.0, blue: 149.0/255.0, alpha: 1.0)
    }

    public func taskViewController(_ taskViewController: ORKTaskViewController, didFinishWith reason: ORKTaskViewControllerFinishReason, error: Error?) {
        //Handle results with taskViewController.result
        taskViewController.dismiss(animated: true, completion: nil)
    }
    
}
